function [COMMUTER] = parking_office(COMMUTER,TIME_SLOT,par)
% PARKING
charge_opportunity=COMMUTER.parkingCharge_opportunity;
if charge_opportunity==true
    energy_charge_perTimeSlot=max(...
        -par.PublicC_powerRate/par.time_slot_hr,...
        -(COMMUTER.EV.ESS.SOEmax-(COMMUTER.EV.ESS.SOE))/par.kW_W);
    COMMUTER.EV.ESS.energyRequest=energy_charge_perTimeSlot*par.kWh_Wh; % Wh
end

tmp_SOE_pu=COMMUTER.EV.ESS.SOE/COMMUTER.EV.ESS.SOEmax; %[pu]
COMMUTER.SOC_trace(TIME_SLOT)=tmp_SOE_pu;
COMMUTER.time_covered=COMMUTER.time_covered+par.time_slot_hr^-1;

if COMMUTER.time_covered>=COMMUTER.parkTime
    COMMUTER.time_covered=[];
    COMMUTER.state='work_home';
    COMMUTER.dist_covered=0;    
end

end