
function [COMMUTER] = FC_deviation(COMMUTER,TIME_SLOT,par)
% DEVIATION TO A FC STATION

space_covered_perTimeSlot=COMMUTER.averageSpeed*par.day_hour/(par.day_timeSlot);
energy_cunsumption=space_covered_perTimeSlot*COMMUTER.EV.ConsumptionRate; % kWh

energy_cunsumption=min(energy_cunsumption,COMMUTER.FCdeviation_energy_demand);
COMMUTER.EV.ESS.energyRequest=energy_cunsumption*par.kWh_Wh; % Wh
tmp_SOE_pu=COMMUTER.EV.ESS.SOE/COMMUTER.EV.ESS.SOEmax; %[pu]
COMMUTER.SOC_trace(TIME_SLOT)=tmp_SOE_pu; % [pu]

COMMUTER.FCdeviation_energy_demand=COMMUTER.FCdeviation_energy_demand-energy_cunsumption;
COMMUTER.FCdeviation_energy_demand=COMMUTER.FCdeviation_energy_demand*(COMMUTER.FCdeviation_energy_demand>0);

if tmp_SOE_pu <= COMMUTER.EV.ESS.SOEmin/COMMUTER.EV.ESS.SOEmax*0.01
    error('SOC state-BOOM!');
end

if COMMUTER.FCdeviation_energy_demand <= 0

    if strcmp(COMMUTER.state,'home_FC_deviation')
        COMMUTER.state=  'FCstop_going2work';
    elseif strcmp(COMMUTER.state,'work_FC_deviation')
        COMMUTER.state=  'FCstop_going2home';
    else
        error('FC deviation')
    end
    COMMUTER.FCdeviation_energy_demand=[];
    COMMUTER.FC_time_covered=0;

end

end