function [COMMUTER] = HomeToWork_WorkToHome(COMMUTER,TIME_SLOT,par,SOC_THRESHOLD,ALFA)
% PATH FROM WORK2HOME AND VICE VERSA 

if isempty(COMMUTER.dist_covered)
    error('error work2Home')
end

dist2cover=COMMUTER.distance-COMMUTER.dist_covered;
dist2cover=dist2cover*(dist2cover>=0);
space_covered_perTimeSlot=COMMUTER.averageSpeed*par.day_hour/(par.day_timeSlot);
space_covered_perTimeSlot=min(space_covered_perTimeSlot,dist2cover);
    
COMMUTER.EV.ESS.energyRequest=space_covered_perTimeSlot*COMMUTER.EV.ConsumptionRate*par.kWh_Wh; % Wh
tmp_SOE_pu=COMMUTER.EV.ESS.SOE/COMMUTER.EV.ESS.SOEmax; %[pu]

COMMUTER.SOC_trace(TIME_SLOT)=tmp_SOE_pu; % [pu]

COMMUTER.dist_covered=space_covered_perTimeSlot+COMMUTER.dist_covered;

if tmp_SOE_pu <= COMMUTER.EV.ESS.SOEmin/COMMUTER.EV.ESS.SOEmax*0.7
    error('SOC state')
end

if tmp_SOE_pu <= SOC_THRESHOLD
    if strcmp(COMMUTER.state,'home_work')
        COMMUTER.state=  'home_FC_deviation';
    elseif strcmp(COMMUTER.state,'work_home')
        COMMUTER.state=  'work_FC_deviation';
    else
        error('error W2H')
    end
    battery_capacity=COMMUTER.EV.ESS.SOEmax/par.kWh_Wh; % kWh
    COMMUTER.FCdeviation_energy_demand=(1-ALFA)*battery_capacity;

elseif COMMUTER.dist_covered>=COMMUTER.distance
    COMMUTER.dist_covered=[];    
    
        if strcmp(COMMUTER.state,'home_work')
            COMMUTER.state=  'parking';
            COMMUTER.time_covered=0;
        elseif strcmp(COMMUTER.state,'work_home')
            COMMUTER.state='home_night';
        end
end

end