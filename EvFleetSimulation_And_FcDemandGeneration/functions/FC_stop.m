function [COMMUTER, STATION] = FC_stop(COMMUTER,TIME_SLOT,par,STATION)
%% FC stop
%% CHECK IF A FC IS ALREADY OCCUPIED BY THE EV
available=0;
extension_waiting_time=16;
% BOOK A FAST CHARGE STATION FOR THE NEXT TIME SLOT
if isempty(COMMUTER.FC_booked) 
    breakagain=false;
    FC_array=randperm(par.num_FC_stations,par.num_FC_stations); % randomize the FC station distribution
    
    expected_energy=(COMMUTER.EV.ESS.SOEmax*par.SOC_recharge_limit_for_FC-COMMUTER.EV.ESS.SOE)/par.kWh_Wh; %kWh
    expected_charging_time= floor(expected_energy/par.FC_powerRate*par.time_slot_hr)+1;
    expected_charging_time= min( expected_charging_time, par.FC_recharge_interval ) ; % time slot

    %FC_array=1:par.num_FC_stations;
    for iter_book_timeslot=TIME_SLOT+1:TIME_SLOT+par.FC_recharge_interval*extension_waiting_time
        timeslot_effective=1+mod(iter_book_timeslot-1,par.day_timeSlot);
        timeslot_array=1+mod(timeslot_effective-1:timeslot_effective+expected_charging_time-1,par.day_timeSlot);
        for iter_FC_selection=1:par.num_FC_stations
            if sum(STATION(FC_array(iter_FC_selection)).EVbook_trace(timeslot_array))==available
                % BOOK THE FC STATION
                COMMUTER.FC_booked=FC_array(iter_FC_selection); % book the FC station
                STATION(COMMUTER.FC_booked).EVbook_trace(timeslot_array(1:end))=COMMUTER.ID;
                COMMUTER.FC_booked=COMMUTER.FC_booked;
                breakagain=true;
                break
            end
        end
        if breakagain==true,break;end
    end
end

%% CHECK BOOKING
if COMMUTER.FC_booked==false || COMMUTER.FC_booked<1
    error('too few FC station')
end

%% CHECK FC AVAILABILITY
% FC-ON
if  STATION(COMMUTER.FC_booked).EVbook_trace(TIME_SLOT)==COMMUTER.ID
    energy_charge_perTimeSlot=-par.FC_powerRate/par.time_slot_hr;
    COMMUTER.EV.ESS.energyRequest=energy_charge_perTimeSlot*par.kWh_Wh; % Wh
    tmp_SOE_pu=COMMUTER.EV.ESS.SOE/COMMUTER.EV.ESS.SOEmax; %[pu]
    COMMUTER.SOC_trace(TIME_SLOT)=tmp_SOE_pu;
    COMMUTER.FC_time_covered=COMMUTER.FC_time_covered+1; 
    % refresh FC PD profile and FC recharging time
    STATION(COMMUTER.FC_booked).PD_trace(TIME_SLOT)=-(energy_charge_perTimeSlot*par.time_slot_hr); %kW
    % record the EV-FC relation
    COMMUTER.FCbook_trace(TIME_SLOT)=COMMUTER.FC_booked;
else % WAIT
    STATION(COMMUTER.FC_booked).PD_trace(TIME_SLOT)=0; %kW    
    tmp_SOE_pu=COMMUTER.EV.ESS.SOE/COMMUTER.EV.ESS.SOEmax; %[pu]
    COMMUTER.SOC_trace(TIME_SLOT)=tmp_SOE_pu;
    COMMUTER.FC_time_covered=0;
    % record the EV-FC relation
    COMMUTER.FCbook_trace(TIME_SLOT)=COMMUTER.FC_booked+0.2;    
end
%% STOP CRITERIA
if tmp_SOE_pu>=par.SOC_recharge_limit_for_FC ||  COMMUTER.FC_time_covered>=par.FC_recharge_interval
    if COMMUTER.FC_time_covered<par.FC_recharge_interval
        % clear all the time slot booked not exploited
        time_slot2clear=1+mod(TIME_SLOT+1:TIME_SLOT+par.FC_recharge_interval-1,par.day_timeSlot);
        time_slot2clear=time_slot2clear(...
            STATION(COMMUTER.FC_booked).EVbook_trace(time_slot2clear)==COMMUTER.ID...
            );
        STATION(COMMUTER.FC_booked).EVbook_trace(time_slot2clear)=available;
    end
    
    %% STEP INTO ANOTHER COMMUTER STATE    
    COMMUTER.FC_time_covered=[];
    COMMUTER.FC_booked=[];
    if strcmp(COMMUTER.state,'FCstop_going2work')
        COMMUTER.state='home_work';
    elseif  strcmp(COMMUTER.state,'FCstop_going2home')
        COMMUTER.state='work_home';
    else
        error('ERROR FC function')
    end
end

end

    % RECHARGE,
    %     energy_charge_perTimeSlot=max(...
    %         -par.FC_powerRate/par.time_slot_hr,...
    %         -(pendolare.EV.ESS.SOEmax*par.SOC_recharge_limit_for_FC-(pendolare.EV.ESS.SOE))/par.kW_W); % kWh


%         figure(99)
%         for iter_FC=1:par.num_FC_stations
%             plot(par.vettore_tempo,(FCstation(iter_FC).EVbook_trace)>0,'or');hold on
%             plot(par.vettore_tempo,(FCstation(iter_FC).PD_trace>0)*2,'k');
%         end