function [COMMUTER] = stayingHome(COMMUTER,TIME_SLOT,par)
% HOME

COMMUTER.SOC_trace(TIME_SLOT)= COMMUTER.SOC_trace(TIME_SLOT-1);
if TIME_SLOT>COMMUTER.departTime*par.time_slot_hr
    COMMUTER.state=  'home_work';
    COMMUTER.dist_covered=0;
end
end