
clear; close all;clc;
import fleet.*

addpath(genpath(horzcat('fleet results')));
addpath(genpath(horzcat('functions')));

load('tmp_File_Name.mat');
load(filename)

par.flagprint=true;
NumOfDay=par.NumOfDay;
day_timeSlot=par.day_timeSlot;
par_FC_colors=(hsv(par.num_FC_stations));

for iter_day=1:NumOfDay

    % FLEET-DAY
        FCstation=myFleet(iter_day,1).FCstations;
        RedCoeff_alfa=myFleet(iter_day,1).RedCoeff_alfa;        
        
    for iter_timeslot=2:day_timeSlot*2
        current_slot=1+mod(iter_timeslot-1,day_timeSlot);

        for iter_vehicle=1:myFleet(iter_day,1).EV_number
            % COMMUTER
            tmpCommuter = myFleet(iter_day, 1).Commuters(iter_vehicle, 1)  ;
            
            switch tmpCommuter.state
                case 'home_morning'
                    tmpCommuter=stayingHome(tmpCommuter,current_slot,par);
                 case 'home_work'
                    tmpCommuter= HomeToWork_WorkToHome(tmpCommuter,current_slot,par,tmpCommuter.SOC_thr_outw ,RedCoeff_alfa);
                case 'home_FC_deviation'
                    tmpCommuter= FC_deviation(tmpCommuter,current_slot,par);
                case 'work_FC_deviation'
                    tmpCommuter= FC_deviation(tmpCommuter,current_slot,par);                    
                case 'parking'
                    tmpCommuter= parking_office(tmpCommuter,current_slot,par);
                case 'work_home'
                    tmpCommuter= HomeToWork_WorkToHome(tmpCommuter,current_slot,par,tmpCommuter.SOC_thr_ret ,RedCoeff_alfa);
                case 'FCstop_going2work'
                    [tmpCommuter,FCstation]= FC_stop(tmpCommuter,current_slot,par,FCstation);
                case 'FCstop_going2home'
                    [tmpCommuter,FCstation]= FC_stop(tmpCommuter,current_slot,par,FCstation);
                case 'home_night'
                    % DO NOTHING
                otherwise
                    error('error State');
            end
            
            myFleet(iter_day, 1).Commuters(iter_vehicle, 1) =tmpCommuter  ;
                    
        end
           fprintf('DAY:%d, TIMESLOT:%d/%d \n',iter_day,iter_timeslot,day_timeSlot);

    end
    myFleet(iter_day,1).FCstations=FCstation;
        %% PRINT
        if par.flagprint==true
            figure(1+iter_day*10)
            subplot(4,1,1)
            for iter_vehicle=1:myFleet(iter_day,1).EV_number
                if isempty(find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1))
                    plot(par.time_array, myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_trace,'LineWidth',1,'Color',[.70 .70 .70]);hold on
                end
            end
            for iter_vehicle=1:myFleet(iter_day,1).EV_number
                if isempty(find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1))
                    % plot(par.time_array, myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_trace,'LineWidth',1,'Color',[.70 .70 .70]);
                else
                    tmp_color=find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1);
                    tmp_color=myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace(tmp_color);
                    tmp_color=floor(tmp_color);
                    tmp_color=par_FC_colors(tmp_color,:);
                    plot(par.time_array, myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_trace,'LineWidth',1,'Color',tmp_color); hold on
                    plot([par.time_array(1) par.time_array(end)],...
                        [myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_outw myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_outw],'-m','LineWidth',.2); hold on
                    plot([par.time_array(1) par.time_array(end)],...
                        [myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_ret myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_ret],'-g','LineWidth',.2); hold on
                end
            end
            title('EV SOC profiles');
            xlim([0 par.time_array(end)]);
            ylabel('SOC')
            xlabel('hour');
            
            %figure(2+iter_day*10)
            for iter_FC_print=1:par.num_FC_stations
                tmp_color=par_FC_colors(iter_FC_print,:);
                subplot(4,1,2)
                plot(par.time_array, myFleet(iter_day,1).FCstations(iter_FC_print,1).PD_trace,'LineWidth',1,'Color',tmp_color); hold on
                xlim([0 par.time_array(end)]);
                title('FC Power Demand');
                ylabel('kW')
                xlabel('hour')
                
                subplot(4,1,3)
                print_index=myFleet(iter_day,1).FCstations(iter_FC_print,1).EVbook_trace>0;
                plot(par.time_array(print_index),par.time_array(print_index)*0+iter_FC_print,...
                    's','LineWidth',2,'MarkerSize',5,...
                    'MarkerEdgeColor', par_FC_colors(iter_FC_print,:),'MarkerFaceColor',tmp_color); hold on
                xlim([0 par.time_array(end)]);
                ylim([0.5 par.num_FC_stations+0.5]);
                title('FC stations activity');
                ylabel('FC station')
                xlabel('hour')
                grid on
            end
            
            figure(1+iter_day*10)
            subplot(4,1,4)
            for iter_vehicle=1:myFleet(iter_day,1).EV_number
                if ~isempty(find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1))
                    for iter_FC_print=1:par.num_FC_stations
                        print_index=find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace==iter_FC_print);
                        print_index_wait=...
                            abs(round(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace)-...
                            myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace);
                        print_index_wait=find(print_index_wait>0);
                        if ~isempty(print_index)
                            tmp_color=par_FC_colors(iter_FC_print,:);
                            plot(par.time_array(print_index), myFleet(iter_day, 1).Commuters(iter_vehicle, 1).FCbook_trace(print_index),...
                                's','LineWidth',2,'MarkerSize',5,'Color',tmp_color); hold on
                        end
                        if ~isempty(print_index_wait)
                            plot(par.time_array(print_index_wait),...
                                myFleet(iter_day, 1).Commuters(iter_vehicle, 1).FCbook_trace(print_index_wait),...
                                '+','LineWidth',2,'MarkerSize',3,'Color',[0 0 0]); hold on
                        end
                    end
                end
            end
            title('FCbook trace');
            xlim([0 par.time_array(end)]);
            ylim([0.5 par.num_FC_stations+0.5]);
            
            grid on
            ylabel('FC station');
            xlabel('hour');
            %%% END PRINT
        end
end


filename=horzcat('SIM_',par.filename);
save( 'tmp_File_Name.mat','filename');
percorsoFile=fullfile('\fleet results',filename);
save( [pwd percorsoFile ], 'myFleet','par');

