clear; close all;clc;
import flotta.*

addpath(genpath(horzcat('fleet results')));
load('tmp_File_Name.mat');
load(filename)

for iter_day=1:par.NumOfDay
    % FLOTTA-DAY
        %FCstation=flotta(iter_day,1).FCstations;
        %RedCoeff_alfa=flotta(iter_day,1).RedCoeff_alfa;        
        %flotta(iter_day,1).FCstations=FCstation;
        %% PRINT
        figure(1+iter_day*1)
        
        subplot(3,1,1)
        for iter_vehicle=1:myFleet(iter_day,1).EV_number
            if isempty(find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1))
                plot(par.vettore_tempo, myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_trace,'LineWidth',1,'Color',[.70 .70 .70]);hold on
            end
        end
        for iter_vehicle=1:myFleet(iter_day,1).EV_number
            if isempty(find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1))
                % plot(par.vettore_tempo, myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_trace,'LineWidth',1,'Color',[.70 .70 .70]);               
            else
                tmp_color=find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1);
                tmp_color=myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace(tmp_color);
                tmp_color=floor(tmp_color);
                tmp_color=par.FC_colors(tmp_color,:);
                plot(par.vettore_tempo, myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_trace,'LineWidth',1,'Color',tmp_color); hold on
                plot([par.vettore_tempo(1) par.vettore_tempo(end)],...
                    [myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_outw myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_outw],'-m','LineWidth',.2); hold on
                plot([par.vettore_tempo(1) par.vettore_tempo(end)],...
                    [myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_ret myFleet(iter_day, 1).Commuters(iter_vehicle, 1).SOC_thr_ret],'-g','LineWidth',.2); hold on
            end
        end
        title('EV SOC profiles');       
        xlim([0 par.vettore_tempo(end)]);
        ylabel('SOC')
        xlabel('hour');
        
        %figure(2+iter_day*10)
        for iter_FC_print=1:par.num_FC_stations
            tmp_color=par.FC_colors(iter_FC_print,:);
            subplot(3,1,2)
            plot(par.vettore_tempo, myFleet(iter_day,1).FCstations(iter_FC_print,1).PD_trace,'LineWidth',1,'Color',tmp_color); hold on
            xlim([0 par.vettore_tempo(end)]);
            title('FC Power Demand');
            ylabel('kW')
            xlabel('hour')
            
            subplot(3,1,3)
            print_index=myFleet(iter_day,1).FCstations(iter_FC_print,1).EVbook_trace>0;
            plot(par.vettore_tempo(print_index),par.vettore_tempo(print_index)*0+iter_FC_print,...
                's','LineWidth',2,'MarkerSize',5,...
                'MarkerEdgeColor', par.FC_colors(iter_FC_print,:),'MarkerFaceColor',tmp_color); hold on
            xlim([0 par.vettore_tempo(end)]);
            ylim([0.5 par.num_FC_stations+0.5]);
            title('FC stations activity');       
            ylabel('FC station')
            xlabel('hour')
            grid on
        end
        
        subplot(3,1,3)
        for iter_vehicle=1:myFleet(iter_day,1).EV_number
            if ~isempty(find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace>0,1))
               for iter_FC_print=1:par.num_FC_stations
                   print_index=find(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace==iter_FC_print);
                   print_index_wait=...
                       abs(round(myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace)-...
                       myFleet(iter_day, 1).Commuters(iter_vehicle).FCbook_trace);
                   print_index_wait=find(print_index_wait>0);
                   if ~isempty(print_index)
                       tmp_color=par.FC_colors(iter_FC_print,:);
                       plot(par.vettore_tempo(print_index), myFleet(iter_day, 1).Commuters(iter_vehicle, 1).FCbook_trace(print_index),...
                           's','LineWidth',2,'MarkerSize',5,'Color',tmp_color); hold on
                   end
                   if ~isempty(print_index_wait)
                       plot(par.vettore_tempo(print_index_wait),...
                           myFleet(iter_day, 1).Commuters(iter_vehicle, 1).FCbook_trace(print_index_wait),...
                           '+','LineWidth',2,'MarkerSize',3,'Color',[0 0 0]); hold on
                   end
                end
            end
        end
        title('FCbook trace');       
        xlim([0 par.vettore_tempo(end)]);
        ylim([0.5 par.num_FC_stations+0.5]);

        grid on        
        ylabel('FC station');
        xlabel('hour');
        %%% END PRINT

end
        nomeFile=(horzcat('fleet_simulated.mat'));
        percorsoFile=fullfile('\fleet results',nomeFile);
        save( [pwd percorsoFile ], 'myFleet','par');