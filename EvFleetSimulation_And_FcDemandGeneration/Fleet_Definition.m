%% definisci fleet
clear; close all;clc
import fleet.*

% fixed values
kW_W=10^3;
hour_day=24;
hour_15min=60/15;
hour_5min=60/5;
hour_1min=60/1;

%% FC STATION PARAMETERS SETTINGS
Num_of_EVs=round(2400);
par.FC_powerRate=50; % Fast Charge station Power Rate [kW]
par.num_FC_stations=30;
par.PublicC_powerRate=6; % kW
par.Home_powerRate=6;    % kW
par.SOC_recharge_limit_for_FC=0.8;
par.FC_recharge_interval=15; % mins
par.FC_colors=(hsv(par.num_FC_stations));
week_array={'mon','tue','wen','thr','fri','mon','tue'};
par.days_array=[repmat(week_array,1,2) week_array(1:1)];
par.NumOfDay=length(par.days_array);
par.filename=horzcat('Fleet_noEV_',num2str(Num_of_EVs),'_noFC_',num2str(par.num_FC_stations),'_noDay_',num2str(length(par.days_array)),'.mat');
par.time_slot_hr=hour_1min;
par.day_timeSlot=par.time_slot_hr*hour_day;
par.NumEV=Num_of_EVs;
par.kW_W=kW_W;
par.kWh_Wh=par.kW_W;
par.day_hour=24;
par.time_array=(1:par.day_timeSlot)/par.time_slot_hr;

%% Home Charging opportunity & Numbers of Commuters
HomeChForgets=[.10,.01]; % [ [%] , sigma[%]) perc. respect to fleet.HomeChOpportunity
HomeChPenetration=0.9;
Commuter_penetration=0.8;
Fulltime_commuter=0.8;
HomeChPenetration=round(Num_of_EVs*HomeChPenetration)/Num_of_EVs; 

%% Reduction Coefficient ALFA
RedCoeff_alfa=0.85;

%% EV features
num_smallSize=round(Num_of_EVs*0.3);
num_mediumSize=round(Num_of_EVs*0.6);
num_largeSize=Num_of_EVs-num_smallSize-num_mediumSize;
ESS_small=[10 20];                  %[kWh, range min e max] % DATA FROM PAPER CELLI
ComsumptionRange_small=[0.10 0.13]; %[kWh, range min e max]-[kWh/km uncertainty]
ESS_medium=[20 30];
ComsumptionRange_medium=[0.15 0.18]; %[kWh, range min e max]-[kWh/km uncertainty]
ESS_large=[30 40];
ComsumptionRange_large=[0.2 0.25]; %[kWh, range min e max]-[kWh/km uncertainty]
SOCthr_outward=[30 1.5]/100; % [mean-sigma], data from paper CELLI
SOCthr_return=[40 2]/100;  %

%% Commuters parking time
parkTime.full=[8  1];    % [h +/- uncertainty]
parkTime.part=[4  0.5];  % [h +/- uncertainty]
parkTime.nonReg=[4 1];   % [h +/- uncertainty]
parkTime.weekend=[1 8];  % [h] uniform distribution between such range
parkTime.minimum=0.5;
parkTime.maximum=9;

parkTime.charge_opportunity_workday=round( Num_of_EVs *0.8); % NUMBER OF VEHICLE
parkTime.charge_opportunity_weekend=round( Num_of_EVs*0.6);   % NUMBER OF VEHICLE

%% Commuter timeDeparture working day
departureTime.full=[8 1.5]; % [h, sigma]
departureTime.part=[11 2.5];% [h, sigma]
departureTime.nonReg=[11 6];% [h, sigma]
departureTime.weekend=[4 19];% [range]
departureTime.minimum=4;
departureTime.maximum=19;

%% Commuter average speed
averageSpeed=[50 10]; %[km/h, sigma]

%% Commuter covered distance
coveredDistance.workday=[30 5];  %[km, sigma] paper 30 5
coveredDistance.weekend=[40 7]; %             paper 40 7
coveredDistance.maximum=80;
coveredDistance.minimum=5;

%% Commuter initial SOC
maxCharge_pu=0.95;
minCharge_pu=0.15;
initialSOC_RechargeAtHome_pu=maxCharge_pu; % [%]

initialSOC_RechargeNotAtHome=[0.6 0.1]; % [% uncertainty]
battery_efficiency=1;
deltaSOEmaximum_pu=maxCharge_pu-minCharge_pu; % sovradimensionato

% Home Charge & initial SOC
HomeChOpportunity_vector=[ones(1,Num_of_EVs*HomeChPenetration ) zeros(1,round(Num_of_EVs*(1-HomeChPenetration)))];
initialSOC_RechargeNotAtHome_array=normrnd(initialSOC_RechargeNotAtHome(1),initialSOC_RechargeNotAtHome(2),round(Num_of_EVs*(1-HomeChPenetration)),1); % pu
vehicleESSInitialSOC_pu_array=[initialSOC_RechargeAtHome_pu*ones(1,Num_of_EVs*HomeChPenetration ) initialSOC_RechargeNotAtHome_array'];
shuffleIndex=randperm(Num_of_EVs);
HomeChOpportunity_vector=HomeChOpportunity_vector(shuffleIndex);
vehicleESSInitialSOC_pu_array=vehicleESSInitialSOC_pu_array(shuffleIndex);
vehicleESSInitialSOC_pu_array(find(vehicleESSInitialSOC_pu_array<(minCharge_pu*1.1)))=minCharge_pu*1.1; % controllo che non vada sotto la soglia minima
vehicleESSInitialSOC_pu_array(find(vehicleESSInitialSOC_pu_array>(maxCharge_pu)))=maxCharge_pu; % controllo che non vada sopra la massima

%% Commuter Type  DT parking TimeDeparture
num_commuter=round(Commuter_penetration*Num_of_EVs);
num_fullTimeCommuter=round(Fulltime_commuter*num_commuter);
num_partTimeCommuter=round((1-Fulltime_commuter)*num_commuter);
num_nonregular_commuter=Num_of_EVs-num_fullTimeCommuter-num_partTimeCommuter;
commuterType_cellarray=cell(Num_of_EVs,1);
commuterType_cellarray(1:num_fullTimeCommuter)= {'fullTime'};   
commuterType_cellarray(num_fullTimeCommuter+1:num_fullTimeCommuter+num_partTimeCommuter)= {'partTime'};   
commuterType_cellarray(end-num_nonregular_commuter+1:end)= {'nonRegular'};
shufflevector_commuter_type=randperm(length(commuterType_cellarray));
commuterType_cellarray=commuterType_cellarray( shufflevector_commuter_type);

%% parking time
% work day
parkingTime_workday_array=[ normrnd(parkTime.full(1),parkTime.full(2),num_fullTimeCommuter,1)
                    normrnd(parkTime.part(1),parkTime.part(2),num_partTimeCommuter,1)
                    normrnd(parkTime.nonReg(1),parkTime.nonReg(2),num_nonregular_commuter,1)
                    ];
parkingTime_workday_array(find(parkingTime_workday_array<parkTime.minimum))=parkTime.minimum;              
parkingTime_workday_array(find(parkingTime_workday_array>parkTime.maximum))=parkTime.maximum;              
parkingTime_workday_array=parkingTime_workday_array(shufflevector_commuter_type);                
% week end
parkingTime_weekend_array=randi(100, Num_of_EVs,1)/100.*( parkTime.weekend(2)-parkTime.weekend(1))+parkTime.weekend(1);
parkingTime_weekend_array(find(parkingTime_weekend_array<parkTime.minimum))=parkTime.minimum;              
parkingTime_weekend_array(find(parkingTime_weekend_array>parkTime.maximum))=parkTime.maximum; 

%% parking charge opportunity
parkCharge_opportunity_workdays=zeros(Num_of_EVs);
parkCharge_opportunity_weekend=zeros(Num_of_EVs);
parkCharge_opportunity_workdays(randperm(Num_of_EVs,parkTime.charge_opportunity_workday))=true;
parkCharge_opportunity_weekend(randperm(Num_of_EVs,parkTime.charge_opportunity_weekend	))=true;

%% time departure
%  workday
timeDeparture_workday_array=[ normrnd(departureTime.full(1),departureTime.full(2),num_fullTimeCommuter,1)
                      normrnd(departureTime.part(1),departureTime.part(2),num_partTimeCommuter,1)
                      normrnd(departureTime.nonReg(1),departureTime.nonReg(2),num_nonregular_commuter,1)
                    ];
                
timeDeparture_workday_array=timeDeparture_workday_array(shufflevector_commuter_type); 
timeDeparture_workday_array(find(timeDeparture_workday_array<departureTime.minimum))=departureTime.minimum;              
timeDeparture_workday_array(find(timeDeparture_workday_array>departureTime.maximum))=departureTime.maximum;              
% weekend
timeDeprture_weekend_array=randi(100, Num_of_EVs,1)/100.*( departureTime.weekend(2)-departureTime.weekend(1))+departureTime.weekend(1);
timeDeprture_weekend_array=timeDeprture_weekend_array(shufflevector_commuter_type); 
timeDeprture_weekend_array(find(timeDeprture_weekend_array<departureTime.minimum))=departureTime.minimum;              
timeDeprture_weekend_array(find(timeDeprture_weekend_array>departureTime.maximum))=departureTime.maximum;              

%% Commuter covered distance
% workday
coveredDistance_workday_array= normrnd(coveredDistance.workday(1),coveredDistance.workday(2),Num_of_EVs,1);
coveredDistance_workday_array(find(coveredDistance_workday_array<coveredDistance.minimum))=coveredDistance.minimum;              
coveredDistance_workday_array(find(coveredDistance_workday_array>coveredDistance.maximum))=coveredDistance.maximum;              
% week end
coveredDistance_weekend_array= normrnd(coveredDistance.weekend(1),coveredDistance.weekend(2),Num_of_EVs,1);
coveredDistance_weekend_array(find(coveredDistance_weekend_array<coveredDistance.minimum))=coveredDistance.minimum;              
coveredDistance_weekend_array(find(coveredDistance_weekend_array>coveredDistance.maximum))=coveredDistance.maximum;    

%% EV size - ESS Capacity and Consumption Range
vehicleSize_cellarray=cell(Num_of_EVs,1);
vehicleBatteryCap_array=zeros(Num_of_EVs,1);
vehicleConsumptionRate_array=zeros(Num_of_EVs,1);
if num_smallSize>0
    vehicleSize_cellarray(1:num_smallSize)= {'small'};
    vehicleBatteryCap_array(1:num_smallSize)= randi(100,num_smallSize,1)/100.*(ESS_small(2)-ESS_small(1)) + ESS_small(1);% battery capacity [kwh]
    vehicleConsumptionRate_array(1:num_smallSize)=randi(100,num_smallSize,1)/100.*(ComsumptionRange_small(2)-ComsumptionRange_small(1)) + ComsumptionRange_small(1); % energy consumption per covered km [kWh/km]
end
if num_mediumSize>0
    vehicleSize_cellarray(num_smallSize+1:num_smallSize+num_mediumSize)= {'medium'};
    vehicleBatteryCap_array(num_smallSize+1:num_smallSize+num_mediumSize)= randi(100,num_mediumSize,1)/100.*(ESS_medium(2)-ESS_medium(1)) + ESS_medium(1);% battery capacity [kwh]
    vehicleConsumptionRate_array(num_smallSize+1:num_smallSize+num_mediumSize)=randi(100,num_mediumSize,1)/100.*(ComsumptionRange_medium(2)-ComsumptionRange_medium(1)) + ComsumptionRange_medium(1); % energy consumption per covered km [kWh/km]
end
if num_largeSize>0
    vehicleSize_cellarray(length(vehicleSize_cellarray)-num_largeSize+1:length(vehicleSize_cellarray))= {'large'};
    vehicleBatteryCap_array(length(vehicleSize_cellarray)-num_largeSize+1:length(vehicleSize_cellarray))= randi(100,num_largeSize,1)/100.*(ESS_large(2)-ESS_large(1)) + ESS_large(1);% battery capacity [kwh]
    vehicleConsumptionRate_array(length(vehicleSize_cellarray)-num_largeSize+1:length(vehicleSize_cellarray))= randi(100,num_largeSize,1)/100.*(ComsumptionRange_large(2)-ComsumptionRange_large(1)) + ComsumptionRange_large(1); % energy consumption per covered km [kWh/km]
end

%% SOC Thresholds outward and return
SOC_thr_outward_array= normrnd(SOCthr_outward(1),SOCthr_outward(2),Num_of_EVs,1);
SOC_thr_return_array = normrnd(SOCthr_return(1),SOCthr_return(2),Num_of_EVs,1);
SOC_thr_outward_array(find(SOC_thr_outward_array<minCharge_pu*1.2))=minCharge_pu*1.2;
SOC_thr_outward_array(find(SOC_thr_outward_array>maxCharge_pu))=maxCharge_pu*0.8;
SOC_thr_return_array(find(SOC_thr_return_array<minCharge_pu*1.2))=minCharge_pu*1.2;
SOC_thr_return_array(find(SOC_thr_return_array>maxCharge_pu))=maxCharge_pu*0.8;

%% Commuter average speed
averageSpeed_array = normrnd(averageSpeed(1),averageSpeed(2),Num_of_EVs,1);

%% FLEET GENERATION
% FC STATIONS GENERATION OBJECTS
for iter_FC=1:par.num_FC_stations
    tmp_FC_st(iter_FC,1)=FCstation(par.FC_powerRate,par.day_timeSlot);
end

for iter_day=1:length(par.days_array)
    giorno=par.days_array(iter_day);
    %% WORK DAY
    if strcmp(giorno,'sab')==false && strcmp(giorno,'dom')==false 
        number_HomeChForgets=round((normrnd(HomeChForgets(1),HomeChForgets(2),1,1))*Num_of_EVs);
        vettore_HomeChForgets=(randperm(length(find(vehicleESSInitialSOC_pu_array==initialSOC_RechargeAtHome_pu)),number_HomeChForgets));
        for iterEV=1:Num_of_EVs
            SOEmax_wh=maxCharge_pu*vehicleBatteryCap_array(iterEV)*kW_W;%[Wh]
            SOEmin_wh=minCharge_pu*vehicleBatteryCap_array(iterEV)*kW_W;%[Wh]
            if sum(iterEV==vettore_HomeChForgets)>0
                forget_InitialSOC_pu=normrnd(initialSOC_RechargeNotAtHome(1),initialSOC_RechargeNotAtHome(2),1,1); % pu
                SOEin_wh=forget_InitialSOC_pu*vehicleBatteryCap_array(iterEV)*kW_W; %[Wh]
            else
                SOEin_wh=vehicleESSInitialSOC_pu_array(iterEV)*vehicleBatteryCap_array(iterEV)*kW_W; %[Wh]
            end
            deltaSOEmaximum_wh=deltaSOEmaximum_pu*vehicleBatteryCap_array(iterEV)*kW_W;%[Wh]
            tmp_Battery=Battery( SOEin_wh,SOEmax_wh,SOEmin_wh,deltaSOEmaximum_wh,[],battery_efficiency,battery_efficiency);
            tmp_Commuter_EV=EV(vehicleConsumptionRate_array(iterEV),tmp_Battery);
            COMMUTER(iterEV,1)=Commuter(...
                iterEV,commuterType_cellarray(iterEV), HomeChOpportunity_vector(iterEV),...
                tmp_Commuter_EV,par.day_timeSlot, SOC_thr_outward_array(iterEV), SOC_thr_return_array(iterEV),...
                averageSpeed_array(iterEV),parkingTime_workday_array(iterEV),timeDeparture_workday_array(iterEV),...
                coveredDistance_workday_array(iterEV),parkCharge_opportunity_weekend(iterEV),[]);
        end
        myFleet(iter_day,1)=Fleet(Num_of_EVs, giorno, HomeChForgets,COMMUTER,tmp_FC_st,parkTime,averageSpeed,coveredDistance,RedCoeff_alfa,par.day_timeSlot);
        clear COMMUTER
    else
        %% WEEK END DAY
        number_HomeChForgets=round((normrnd(HomeChForgets(1),HomeChForgets(2),1,1))*Num_of_EVs);
        vettore_HomeChForgets=(randperm(length(find(vehicleESSInitialSOC_pu_array==initialSOC_RechargeAtHome_pu)),number_HomeChForgets));
        for iterEV=1:Num_of_EVs
            SOEmax_wh=maxCharge_pu*vehicleBatteryCap_array(iterEV)*kW_W;%[Wh]
            SOEmin_wh=minCharge_pu*vehicleBatteryCap_array(iterEV)*kW_W;%[Wh]
            if sum(iterEV==vettore_HomeChForgets)>0
                forget_InitialSOC_pu=normrnd(initialSOC_RechargeNotAtHome(1),initialSOC_RechargeNotAtHome(2),1,1); % pu
                SOEin_wh=forget_InitialSOC_pu*vehicleBatteryCap_array(iterEV)*kW_W; %[Wh]
            else
                SOEin_wh=vehicleESSInitialSOC_pu_array(iterEV)*vehicleBatteryCap_array(iterEV)*kW_W; %[Wh]
            end
            deltaSOEmaximum_wh=deltaSOEmaximum_pu*vehicleBatteryCap_array(iterEV)*kW_W;%[Wh]
            tmp_Battery=Battery( SOEin_wh,SOEmax_wh,SOEmin_wh,deltaSOEmaximum_wh,[],battery_efficiency,battery_efficiency);
            tmp_Commuter_EV=EV(vehicleConsumptionRate_array(iterEV),tmp_Battery);
            COMMUTER(iterEV,1)=Commuter(...
                iterEV,commuterType_cellarray(iterEV), HomeChOpportunity_vector(iterEV),...
                tmp_Commuter_EV, par.day_timeSlot,SOC_thr_outward_array(iterEV),  SOC_thr_return_array(iterEV),...
                averageSpeed_array(iterEV),parkingTime_weekend_array(iterEV),timeDeprture_weekend_array(iterEV),...
                coveredDistance_weekend_array(iterEV), parkCharge_opportunity_weekend(iterEV),[] );
        end
        myFleet(iter_day,1)=Fleet(Num_of_EVs, giorno, HomeChForgets,COMMUTER,tmp_FC_st,parkTime,averageSpeed,coveredDistance,RedCoeff_alfa,par.day_timeSlot);
    end
end

filename=par.filename;
save( 'tmp_File_Name.mat','filename');
pathfile=fullfile('\fleet results',filename);
save( [pwd pathfile ], 'myFleet','par');

clear all; clc;

EV_fleet_simulation
FC_Dataset_Generation