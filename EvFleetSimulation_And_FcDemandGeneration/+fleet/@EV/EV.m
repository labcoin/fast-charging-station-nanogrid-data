
classdef EV
properties
        ConsumptionRate;
        ESS;
    end
    
    methods
        % Constructor
        function obj = EV( EV_ConsumptionRate, battery_obj)
        
        if  ~isempty(EV_ConsumptionRate)
        obj.ConsumptionRate=EV_ConsumptionRate;
        end
        
        if  ~isempty(battery_obj)
        obj.ESS=battery_obj;
        end
        
        end
    end
end