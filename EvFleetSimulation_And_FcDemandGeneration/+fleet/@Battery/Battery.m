%% OGGETTO BATTERIA 
%% operatore energyRequest: once the batter energy exchange is define, provide to effectively execute this action of charging/discharging.

classdef Battery
    properties
        energyRequest;
        SOEin;
        SOEmax;
        SOEmin;
        SOEfin;
        deltaSOEmaxDCH;
        deltaSOEmaxCH;
        SOE;
        energiaRete;
        etaCh;
        etaDch;
        charge_tr;
        ES_tr;
    end
    methods
     % metodo costruttore
        function     obj=Battery(SOEiniz,SOEmaxim,SOEminim,deltaSOEmaxim,SOEfin,etaCh,etaDch)
            obj.SOEin=SOEiniz;
            obj.SOEmax=SOEmaxim;
            obj.SOEmin=SOEminim;
            obj.deltaSOEmaxDCH=deltaSOEmaxim;
            obj.deltaSOEmaxCH=-deltaSOEmaxim;
            obj.SOE=SOEiniz;
            obj.SOEfin=SOEfin;
            obj.etaCh=etaCh;
            obj.etaDch=etaDch;
        end  
        
     %% method set  
     % Function: energy flow management 
     % INPUT: Energy Demand     
        function obj=set.energyRequest(obj,energia)
            obj.energyRequest=energia;         % Energia richiesta [Wh]
            
            if energia >= 0 % scarica
                efficiency=obj.etaDch;
                maxDCH= (obj.SOE-obj.SOEmin*.01)*efficiency;
                obj.energyRequest = min([energia, obj.deltaSOEmaxDCH, maxDCH]) ;
            else            % <0 ,carica
                efficiency=1/obj.etaCh;
                minCH= -(obj.SOEmax-obj.SOE)*efficiency;
                obj.energyRequest = max([energia, obj.deltaSOEmaxCH, minCH]) ; 
            end
            obj.energiaRete=-(obj.energyRequest-energia);
            obj.SOE=obj.SOE - obj.energyRequest/efficiency;
            if obj.SOE<=0
                warning('BOOM! (BATTERY)')
            end
        end
        
    end
end

                     
            
         

