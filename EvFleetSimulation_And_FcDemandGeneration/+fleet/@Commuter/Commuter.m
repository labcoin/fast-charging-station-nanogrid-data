classdef Commuter
    properties
        ID;
        type;
        homeCharge;
        EV;
        SOC_trace;
        SOC_thr_outw;
        SOC_thr_ret;
        averageSpeed;
        parkTime;
        time_covered;        
        departTime;
        distance;
        state;
        parkingCharge_opportunity;
        dist_covered;
        FCdeviation_energy_demand;  
        FC_time_covered;
        FCbook_trace;
        FC_booked;
    end
    
    methods
        % Constructor
        function obj = Commuter(ID,Commuter_Type, Commuter_HomeCharge, Commuter_EV,numberTslot, SOCoutward,SOCreturn,averageSpeedval,...
                parkingTime,timeDeparture, distance,Commuter_parkCharge, initialState )
            obj.ID=ID;
            obj.SOC_trace=zeros(1,numberTslot);
            obj.FCbook_trace=zeros(1,numberTslot);
            obj.SOC_trace(1)=Commuter_EV.ESS.SOEin/Commuter_EV.ESS.SOEmax;
            obj.type=Commuter_Type;
            obj.homeCharge=Commuter_HomeCharge;
            obj.EV=Commuter_EV;
            obj.SOC_thr_outw=SOCoutward;
            obj.SOC_thr_ret=SOCreturn;
            obj.averageSpeed=averageSpeedval;
            obj.parkTime=parkingTime;
            obj.departTime=timeDeparture;
            obj.distance=distance;
            obj.parkingCharge_opportunity=Commuter_parkCharge;
            if isempty(initialState)
            obj.state='home_morning';
            else
            obj.state=initialState;
            end            
        end
                
    end
end