classdef FCstation
    properties
        Power;
        PD_trace;
        EVbook_trace;
    end
    
    methods
        % Constructor
        function obj = FCstation(power,numberTslot)
            obj.Power=power;
            obj.PD_trace=zeros(1,numberTslot); %kW
            obj.EVbook_trace=zeros(1,numberTslot); % the EV ID               
        end
    end
end