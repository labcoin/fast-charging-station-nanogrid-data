
classdef Fleet
properties
        EV_number;
        day;
        timeslot_per_day;
        HomeChForgets;
        Commuters;
        parking_Time;
        averageSpeed;
        coveredDistance;
        RedCoeff_alfa;
        FCstations;
    end
    
    methods
        % Constructor
        function obj = Fleet(EV_number, day, HomeChForgets,Commuters,FCstations,parking_Time,averageSpeed,coveredDistance,RedCoeff_alfa,timeSlots)
            obj.EV_number=EV_number;
            obj.day=day;
            obj.timeslot_per_day=timeSlots;
            obj.HomeChForgets=HomeChForgets;
            obj.Commuters=Commuters;
            obj.parking_Time=parking_Time;
            obj.averageSpeed=averageSpeed;
            obj.coveredDistance=coveredDistance;
            obj.RedCoeff_alfa=RedCoeff_alfa;
            obj.FCstations=FCstations;
        end
    end
end