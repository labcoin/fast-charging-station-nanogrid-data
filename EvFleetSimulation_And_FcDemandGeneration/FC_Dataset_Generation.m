clear; close all;clc;
import fleet.*

addpath(genpath(horzcat('fleet results')));
load('tmp_File_Name.mat');
load(filename)

FC_ID_selected=[13 22 7 28 1 18 5 20]; % FC ID FOR DATASET GENERATION
numOfFC=length(FC_ID_selected);

newDayTimeslot=par.day_hour*60/15;
timeSlotResolution=round(par.day_timeSlot/newDayTimeslot);

timeVector_old=par.time_array;
matrix_time_old=zeros(par.NumOfDay,par.day_timeSlot);
timeVector_new=linspace(0,24,newDayTimeslot);
matrix_time_new=zeros(par.NumOfDay,par.day_timeSlot/timeSlotResolution);

for iter_FC=1:numOfFC
    profile(iter_FC).matrix_FC=zeros(par.NumOfDay,par.day_timeSlot);
    profile(iter_FC).matrix_FC_simData=zeros(par.NumOfDay,par.day_timeSlot/timeSlotResolution);
end

for iterDay=1:par.NumOfDay
    matrix_time_old(iterDay,:)=timeVector_old+(iterDay-1)*timeVector_old(end);
    matrix_time_new(iterDay,:)=timeVector_new+(iterDay-1)*timeVector_new(end);
    
    for iter_FC=1:numOfFC
        profile(iter_FC).matrix_FC(iterDay,:)=myFleet(iterDay, 1).FCstations(FC_ID_selected(iter_FC), 1).PD_trace;        
        for iter_new_TS=1 : newDayTimeslot
            in=1+((iter_new_TS-1)*timeSlotResolution);
            fin=((iter_new_TS)*timeSlotResolution);
            profile(iter_FC).matrix_FC_simData(iterDay,iter_new_TS)=(mean(profile(iter_FC).matrix_FC(iterDay,in:fin))); % kWh
        end
        %     figure
        %      plot(timeVector_old,profile(iter_FC).matrix_FC_profile(iterDay,:),'k'); hold on
        %      plot(timeVector_new,profile(iter_FC).matrix_FC_profile_simData(iterDay,:),'r'); hold on
    end
end


matrix_time_old=reshape(matrix_time_old',1,[])/par.day_hour;
matrix_time_new=reshape(matrix_time_new',1,[])/par.day_hour;


for iter_FC=1:numOfFC
    profile(iter_FC).FC_power_vect=reshape(profile(iter_FC).matrix_FC',1,[]);
    profile(iter_FC).FC_power_simData_vect=reshape(profile(iter_FC).matrix_FC_simData',1,[]);
    profile(iter_FC).FC_power_simData_refined=round(profile(iter_FC).FC_power_simData_vect/par.FC_powerRate).*par.FC_powerRate;
    profile(iter_FC).FC_energy_simData_refined=profile(iter_FC).FC_power_simData_refined*par.day_hour/newDayTimeslot;
end

H=[];

for iter_FC=numOfFC:-1:1
H(iter_FC)=figure(iter_FC);
subplot(2,1,1)
plot(matrix_time_old,profile(iter_FC).FC_power_vect,'Color',[0.7 0.7 0.7]); hold on
plot(matrix_time_new,profile(iter_FC).FC_power_simData_vect,'sr'); hold on
plot(matrix_time_new,profile(iter_FC).FC_power_simData_refined,'sb'); hold on
legend('FC power demand-1min','FC average power demand-15min','FC refined power demand-15min')
title(horzcat('FC station',num2str(iter_FC)',', Power Demand'));
xlabel('day');
ylabel('kW');

subplot(2,1,2)
plot(matrix_time_new,profile(iter_FC).FC_energy_simData_refined,'-b'); hold on
title(horzcat('FC station',num2str(iter_FC)',', Energy Demand'))
legend('Energy demand');
xlabel('day');
ylabel('kWh');
end

dataset.FC=[profile(1).FC_energy_simData_refined];
dataset.time_array=[matrix_time_new];
if numOfFC>1
    for iter_FC=2:numOfFC
        dataset.FC=[dataset.FC, profile(iter_FC).FC_energy_simData_refined];
        dataset.time_array=[dataset.time_array, (matrix_time_new+dataset.time_array(end))];
    end
end

H(numOfFC+1)=figure(numOfFC+1);
plot(dataset.time_array,dataset.FC,'-b'); hold on
title(horzcat('FC station Dataset'));
xlabel('day');
ylabel('kWh');

filename=(horzcat('dataset_extended_',par.filename));
filePath=fullfile('\fleet results',filename);
save( [pwd filePath ], 'profile','par','dataset');

filename=(horzcat('figure_extended',par.filename(1:end-3),'fig'));
filePath=fullfile('\fleet results',filename);
saveas(H,[pwd filePath]);
