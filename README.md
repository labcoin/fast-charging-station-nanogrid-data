# Fast Charging Station Nanogrid Data

![](https://img.shields.io/badge/Language-Matlab-green)   ![](https://img.shields.io/badge/License-CC4.0-blue)

This repository contains the dataset developed for the tests published in the paper "Intelligent Energy Flow Management of a Nanogrid Fast Charging Station Equipped with Second Life Batteries" and a mixed
deterministic-stochastic tool to simulate the (fast charging) EV energy demand which has been used to reproduce the (fast) charging station energy demand time series.

The repository contains the following files:
- PublicFCStationNanogrid: a .mat file where are shared the dataset used in the manuscript.
- PublicFCStationNanogrid_dataset_08Sept2020: a pdf file where is detailed the dataset shared.
- EvFleetSimulation_And_FcDemandGeneration: this folder conatins the tool used for the electric vehicle fleet simulation and fast charging station demand generation. 

If you use the files of this repository for scientific purposes, please consider citing [our paper](...):
```
@article{LEONORI2021106602,
title = "Intelligent energy flow management of a nanogrid fast charging station equipped with second life batteries",
journal = "International Journal of Electrical Power & Energy Systems",
volume = "127",
pages = "106602",
year = "2021",
issn = "0142-0615",
doi = "https://doi.org/10.1016/j.ijepes.2020.106602",
url = "http://www.sciencedirect.com/science/article/pii/S0142061520313788",
author = "Stefano Leonori and Giorgio Rizzoni and Fabio Massimo {Frattale Mascioli} and Antonello Rizzi",
keywords = "Energy management system, Fast charging, Fuzzy inference systems, Machine learning, Nanogrids",
}
```

**Note:** a detailed user guide is under construction.
